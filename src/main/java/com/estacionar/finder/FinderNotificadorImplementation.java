package com.estacionar.finder;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import com.estacionar.interfaz.Notificador;

public class FinderNotificadorImplementation {
	
	public FinderNotificadorImplementation() {
		
	}
	
//	public NotificadorMail fisrtImplementation(String path, String paquete) {
//		Set<Object> result = findClasses(path, paquete);
//		List<Object> listadoDeImplementaciones = new ArrayList<Object>(result);
//		return (NotificadorMail) listadoDeImplementaciones.get(0);
//	}
	
	@SuppressWarnings({ "all" })
	public	Set<Object> findClasses(String path, String paquete) {
		Set<Object> result = new HashSet<Object>();
		
		for (File f : new File(path).listFiles()) {
			String nombre = f.getName();
			
			System.out.println(nombre);
			
			if (!nombre.endsWith(".class")) 
				continue;
			
			Class c = null;
			
			try {
				String[] nombreClase = nombre.split("\\.");
				c = Class.forName(paquete+nombreClase[0]);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			if (!Notificador.class.isAssignableFrom(c))
				throw new RuntimeException();
			try {
				result.add( c.newInstance() );
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}

	
}
